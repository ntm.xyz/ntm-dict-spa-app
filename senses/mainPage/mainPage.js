import React from 'react';
import { connect } from 'react-redux';
import {
  Page,
  Splitter,
  SplitterContent,
  ProgressBar
} from 'react-onsenui';

import NavBar from '../../components/NavBar';
import SideMenu from '../../components/SideMenu';

import Styles from './mainPage.scss';
import { WordInfo } from '../../components/wordInfo/wordInfo';
import { fetchWord } from './mainPage.action';

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  renderToolBar = () => {
    const { isLoading } = this.props;
    return <div>
      <NavBar
        title='OX Dict'
        navigator={navigator}
        onMenuClick={this.onToolBarMenuClick}
        onSearch={this.onSearch}
      />
      { isLoading && <div className={Styles.processBarWrapper}>
        <ProgressBar indeterminate />
      </div>}
    </div>;
  }

  onSideMenuOpen = () => {
    this.setState({
      isOpen: true
    });
  };

  onSideMenuClose = () => {
    this.setState({
      isOpen: false
    });
  };

  onToolBarMenuClick = () => {
    this.setState({
      isOpen: true
    });
  };

  onSearch = (searchString) => {
    const { searchWord } = this.props;
    searchWord(searchString);
  };

  render() {
    const { word } = this.props;
    return <Splitter>
      <SideMenu
        isOpen={this.state.isOpen}
        onClose={this.onSideMenuClose}
        onOpen={this.onSideMenuOpen}
      />
      <SplitterContent>
        <Page renderToolbar={this.renderToolBar}>
          <div className={Styles.container}>
            { !!word && <WordInfo word={word} /> }
          </div>
        </Page>
      </SplitterContent>
    </Splitter>;
  }
}

const mapStateToProps = (state) => {
  const { word, isLoading } = state.MainPageSearchReducer;
  return {
    isLoading,
    word
  };
};
const mapDispatchToProps = (dispatch) => ({
  searchWord: (word) => dispatch(fetchWord(word))
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
