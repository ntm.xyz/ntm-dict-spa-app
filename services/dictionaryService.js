import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { getTranslationUrl } from '../config/serviceUrl';

export const lookupWord = (word) => {
  const url = getTranslationUrl('en', word);
  return ajax.getJSON(url).pipe(
    map(response => response.results[0])
  );
};
