const localStorage = window.localStorage;

class Store {
  constructor() {
    const data = localStorage.getItem('__DB__');
    this.data = {...data};
  }

  set(key, val) {
    this.data[key] = val;
    return this;
  }

  get(key) {
    return this.data[key];
  }

  remove(key) {
    delete this.data[key];
    return this;
  }
}

export const resentWords = new Store();
