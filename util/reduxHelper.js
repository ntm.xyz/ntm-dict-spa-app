import { forEach, noop } from 'lodash';
import camelCase from 'camelcase';

export const createAction = (name, path = 'ROOT') => {
  const actions = ['START', 'SUCCESS', 'FAILURE', 'CANCEL'];
  const ret = {};
  forEach(actions, (action) => {
    const actionValue = `${path}/${name}_${action}`;
    ret[camelCase(`${action}`)] = actionValue;
    ret[camelCase(`create_${action}`)] = (payload) => ({
      type: actionValue,
      payload: payload
    });
  });
  return ret;
};

export const createReducer = (name, actions, { startCb, successCb, failureCb, cancelCb, defaultState }) => {
  return (state = defaultState, action) => {
    switch (action.type) {
      case actions.start:
        return startCb(state, action);
      case actions.success:
        return (successCb || noop)(state, action);
      case actions.failure:
        return (failureCb || noop)(state, action);
      case actions.cancel:
        return (cancelCb || noop)(state, action);
    }
    return state;
  };
};
