import React from 'react';

import {
  SearchInput
} from 'react-onsenui';

export class SearchBar extends React.Component {
  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.props.onSearch(e.target.value);
    }
  };

  render() {
    return <div>
      <SearchInput placeholder='Search Dictionary...' onKeyPress={this.onKeyPress} />
    </div>;
  }
}
