import React from 'react';
import Styles from './NavBar.scss';

import {
  Toolbar,
  ToolbarButton,
  Icon
} from 'react-onsenui';
import { SearchBar } from './searchbar/SearchBar';

const NavBar = ({title, navigator, backButton, onMenuClick, onSearch}) => {
  return <div className={Styles.navBar}>
    <Toolbar>
      <div className='left'>
        <SearchBar onSearch={onSearch} />
      </div>
      <div className='right'>
        <ToolbarButton onClick={onMenuClick}>
          <Icon icon='ion-navicon, material:md-menu'/>
        </ToolbarButton>
      </div>
    </Toolbar>
  </div>;
};

export default NavBar;
