import React from 'react';

import Styles from './wordInfo.scss';

export class Sense extends React.PureComponent {
  render() {
    const { sense } = this.props;
    return <li>
      {sense.definitions && sense.definitions.map((definition, index) => <p key={index}>{definition}</p>)}
      {sense.examples && sense.examples.map((example, index) =>
        <blockquote className={Styles.example} key={index}>{example.text}</blockquote>
      )}
    </li>;
  }
};
