import React from 'react';
import {
  SplitterSide,
  Page,
  List,
  ListItem
} from 'react-onsenui';

const SideMenu = ({isOpen, onOpen, onClose}) => (<SplitterSide
  style={{
    boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)'
  }}
  side='right'
  width={200}
  collapse={true}
  swipeable={true}
  isOpen={isOpen}
  onClose={onClose}
  onOpen={onOpen}
>
  <Page>
    <List
      dataSource={['Profile', 'Settings']}
      renderRow={(title) => (
        <ListItem key={title} tappable>{title}</ListItem>
      )}
    />
  </Page>
</SplitterSide>
);

export default SideMenu;
